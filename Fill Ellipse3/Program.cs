﻿using System;
using System.Drawing;
using System.Windows.Forms;

class FillE : Form
{
    Color colorEllipse = Color.Red;
    bool bFill = true; //= false;

    public static void Main()
    { Application.Run (new FillE()); }

public FillE ()
{
    Text = "draw or fill";
    ResizeRedraw = true;

    Menu = new MainMenu();
    Menu.MenuItems.Add("&Options");
    Menu.MenuItems[0].MenuItems.Add("&Color...", new EventHandler (MenuColorOnClick));
}
void MenuColorOnClick (object obj, EventArgs ea)
{
    ColFillDlgBox dlg = new ColFillDlgBox();
    dlg.ShowApply = true;
    dlg.Apply += new EventHandler(dlg_Apply);
    dlg.Color = colorEllipse;
    dlg.Fill = bFill;

    if (dlg.ShowDialog() == DialogResult.OK)
    {
        colorEllipse = dlg.Color;
        bFill = dlg.Fill;
        Invalidate();
    }
}

void dlg_Apply(object sender, EventArgs e)
{
    ColFillDlgBox dlg = (ColFillDlgBox)sender;
    colorEllipse = dlg.Color;
    bFill = dlg.Fill;
    Invalidate();
}
protected override void OnPaint(PaintEventArgs e)
{
    Graphics g = e.Graphics;
    Rectangle rect = new Rectangle(0, 0, ClientSize.Width - 1, ClientSize.Height - 1);
    if (bFill)
        g.FillEllipse(new SolidBrush(colorEllipse), rect);
    else
        g.DrawEllipse(new Pen(colorEllipse), rect);
}
}

class ColFillDlgBox : Form
{
    protected GroupBox grpbox;
    protected CheckBox chkbox;
    Button btnApply;
    public event EventHandler Apply;

    public ColFillDlgBox()
    {
        Text = "color/fill select";
        FormBorderStyle = FormBorderStyle.FixedDialog;
        ControlBox = false;
        MinimizeBox = false;
        MaximizeBox = false;
        ShowInTaskbar = false;
        Location = ActiveForm.Location + SystemInformation.CaptionButtonSize + SystemInformation.FrameBorderSize;

        string[] astrColor = { "Black", "Blue", "Green" };

        grpbox = new GroupBox();
        grpbox.Parent = this;
        grpbox.Text = "Color";
        grpbox.Location = new Point(36, 8);
        grpbox.Size = new Size(96, 12 * (astrColor.Length + 1));

        for (int i = 0; i < astrColor.Length; i++)
        {
            RadioButton rdb = new RadioButton();
            rdb.Parent = grpbox;
            rdb.Text = astrColor[i];
            rdb.Location = new Point(8, 12 * (i + 1));
            rdb.Size = new Size(80, 10);
        }
        chkbox = new CheckBox();
        chkbox.Parent = this;
        chkbox.Text = "fill ellipse";
        chkbox.Location = new Point(120, grpbox.Bottom + 4);
        chkbox.Size = new Size(80, 10);

        Button btn = new Button();
        btn.Parent = this;
        btn.Text = "OK";
        btn.Location = new Point(8, chkbox.Bottom + 4);
        btn.Size = new Size(40, 16);
        btn.DialogResult = DialogResult.OK;
        AcceptButton = btn;

        Button btn2 = new Button();
        btn2.Parent = this;
        btn2.Text = "cancel";
        btn2.Location = new Point(64, chkbox.Bottom + 4);
        btn2.Size = new Size(40, 16);
        btn2.DialogResult = DialogResult.Cancel;
        CancelButton = btn2;

        btnApply = new Button();
        btnApply.Parent = this;
        btnApply.Enabled = false;
        btnApply.Text = "apply";
        btnApply.Location = new Point(120, chkbox.Bottom + 4);
        btnApply.Size = new Size(40, 16);
        btnApply.Click += new EventHandler(btnApplyOnClick);

        ClientSize = new Size(168, btn.Bottom + 8);
        AutoScaleBaseSize = new Size(4, 8);
    }
    public Color Color
    {
        get
        {
            for (int i = 0; i < grpbox.Controls.Count; i++)
            {
                RadioButton rdb = (RadioButton)grpbox.Controls[i];
                if (rdb.Checked)
                    return Color.FromName(rdb.Text);
            }
            return Color.Black;
        }
        set
        {
            for (int i = 0; i < grpbox.Controls.Count; i++)
            {
                RadioButton rdb = (RadioButton)grpbox.Controls[i];
                if (value == Color.FromName(rdb.Text))
                {
                    rdb.Checked = true;
                    break;
                }
            }
        }
    }
    public bool Fill
    {
        get { return chkbox.Checked; }
        set { chkbox.Checked = value; }
    }

    public bool ShowApply
    {
        get { return btnApply.Enabled; }
        set { btnApply.Enabled = value; }
    }
    void btnApplyOnClick(object o, EventArgs ea)
    {
        if (Apply != null)
            Apply(this, new EventArgs());
    }
}

